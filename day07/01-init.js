/**
 * Created by 13258 on 2017/6/15.
 */
// 1.引包
var mongoose = require('mongoose');
// 2.连接数据库
mongoose.connect('mongodb://localhost/mongoose');
// 3.创建了一个模型，猫的模型。所有的猫都有名字，是字符串。‘类’
var Cat = mongoose.model('Cat', { name: String });
// 4.实例化一只猫
var kitty = new Cat({ name: 'Zildjian' });
// 5.调用这只猫的save方法，保存这只猫
kitty.save(function (err) {

    console.log('喵喵');
});

var tom = new Cat({name:'汤姆'});
tom.save(()=>{
    console.log('111')
})