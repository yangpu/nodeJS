/**
 * Created by 13258 on 2017/6/15.
 */
// 1.引包
var mongoose = require('mongoose');
// 2.创建数据库连接
var db       = mongoose.createConnection('mongodb://127.0.0.1:27017/NodeJS');
// 3链接错误
db.on('error', function(error) {
    console.log(error);
});
// 4.监听open事件
db.once('open', function() {
    // we're connected!
    console.log('数据库成功连接')
});
// 向外暴露这个db对象
module.exports = db