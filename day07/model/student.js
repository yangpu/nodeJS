/**
 * Created by 13258 on 2017/6/15.
 * var mongooseSchema = new mongoose.Schema({
    username : {type : String, default : '匿名用户'},
    title    : {type : String},
    content  : {type : String},
    time     : {type : Date, default: Date.now},
    age      : {type : Number}
});
 */
// 1.Schema 结构
var mongoose = require('mongoose');
var db = require('./db.js')

// 2.创建了一个schema结构
var mongooseSchema = new mongoose.Schema({
    name     : {type : String},
    age      : {type : Number},
    sex       : {type: String}
});
// 3.创建了一个学生模型，学生类
// 类是基于scheme创建
var mongooseModel = db.model('Student', mongooseSchema);
module.exports =  mongooseModel
