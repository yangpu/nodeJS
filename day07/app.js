/**
 * Created by 13258 on 2017/6/15.
 */
// 定义了一个模型，学生模型，‘学生类’
var Student = require('./model/student.js')
// 实例化一个学生类
var Tom = new Student({name:'汤姆',age:12,sex: '男'})
Tom.save(function(error) {
    if(error) {
        console.log(error);
    } else {
        console.log('saved OK!');
    }
    // 关闭数据库链接
    db.close();
});