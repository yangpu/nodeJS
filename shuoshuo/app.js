/**
 * Created by 13258 on 2017/5/18.
 */
var express = require('express');
var app = express();
var router = require('./router/index')

// 模板引擎
app.set('view engine', 'ejs')
// 静态页面
app.use(express.static('./public'))
// 路由表
app.get('/',router.showIndex)

app.listen(3000)
