var http = require('http');
var fs = require('fs');

var server = http.createServer((req,res) => {
	// 不处理小图标
	if (req.url === '/favicon.ico') {
		return;
	}
	fs.readdir("./album/",(err,file) => {
		// files是存放文件交的数组
		// 存放文件夹的数组
		var dictory = [];
		(function iterator (i) {
			if (i === file.length) {
				console.log(dictory)
				return;
			}
			fs.stat('./album/'+file[i],(err,stats) => {
				if (stats.isDirectory()) {
					dictory.push(file[i])
				}
				iterator(i+1)
			})
		})(0)
	})
	res.end()
})
server.listen(3000,'localhost')