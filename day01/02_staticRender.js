// require表示引包，引包就是引用自己的一个特殊功能
var http = require('http');
var fs = require('fs')

// 创建服务器，参数是一个回调函数，表示如果有请求进来，要做什么
var server = http.createServer((req,res) => {
  // req表示请求，request； res表示响应，respond
  // 设置HTTP头部，状态码是200，文件类型是html，字符集是utf-8
  if(req.url === "/fang") {
    fs.readFile("./static/test.html",(err,data) => {
      res.writeHead(200,{"Content-type":"text/html;charset=UTF-8"});
      res.end(data);
    })
  }else if (req.url === "/yuan") {
    fs.readFile("./static/yuan.html",(err,data) => {
      res.writeHead(200,{"Content-type":"text/html;charset=UTF-8"});
      res.end(data);
    })
  }else if (req.url === "/1.jpg") {
    fs.readFile("./static/1.jpg",(err,data) => {
      res.writeHead(200,{"Content-type":"image/jpg"});
      res.end(data);
    })
  }else if (req.url === "/bbb.css") {
    fs.readFile("./static/aaa.css",(err,data) => {
      res.writeHead(200,{"Content-type":"text/css"});
      res.end(data);
    })
  }else {
    res.writeHead(404,{"Content-type":"text/html;charset=UTF-8"});
    res.end("嘻嘻，没有这个页面哦");
  }
  
  
})

// 运行服务器，监听3000端口
server.listen(3000,"127.0.0.1");