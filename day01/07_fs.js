var http = require('http');
var fs = require('fs');

var server = http.createServer((req,res) => {
	if (req.url === '/favicon.ico') {
		return;
	}
  res.writeHead(200,{'Content-type':'text/html;charset:UTF8'})
  fs.readFile('./static/1.txt',(err,data) =>{
  	if(err) {
  		throw err
  	}
  	res.end(data)
  })
})
server.listen(3000,'localhost')