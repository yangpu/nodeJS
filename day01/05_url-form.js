var http = require('http');
var url = require('url');

var server = http.createServer((req,res) => {
  // 得到查询部分，由于写了true，那么就是一个对象
  var queryObj = url.parse(req.url,true).query;
  var account = queryObj.account;
  var age = queryObj.age;
  var sex = queryObj.sex;
  console.log(`account:${account}<======>age:${age}<======>sex:${sex}`)
  res.end(`account:${account}<======>age:${age}<======>sex:${sex}`)
})
server.listen(3000,'localhost');