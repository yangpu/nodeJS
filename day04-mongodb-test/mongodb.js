/**
 * Created by 13258 on 2017/5/14.
 */
var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/mongodbConect';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db.close();
});
//没连接的错误
// AssertionError: null == {
// MongoError: failed to connect to server [localhost:27017] on first connect
// [MongoError: connect ECONNREFUSED 127.0.0.1:27017]