/**
 * Created by 13258 on 2017/5/21.
 */
var http = require('http');
var fs = require('fs')
var server = http.createServer(function (req,res) {
    if (req.url === '/') {
        // 显示首页
        fs.readFile('./index.html',function (err,data) {
            res.end(data)
        })
    }
})
// 创建一个io对象
var io = require('socket.io')(server)
// 监听连接事件
io.on('connection',function (socket) {
    console.log('1个客户端连接了')
    // 监听
    socket.on('tiwen',function (msg) {
        // console.log('我得到了一个提问' + msg)
        // 回答
        // socket.emit('huida', msg)
        // 广播全部都有
        io.emit('huida', msg)
    })
})

server.listen(3000, 'localhost')