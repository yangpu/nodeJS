var express = require('express')
var app = express()

// socket公式
var http = require('http').Server(app)
var io = require('socket.io')(http)

// session公式
var session = require('express-session')
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized:true
}))


var alluser = []

app.set('view engine', 'ejs')

app.use(express.static('./public'));

app.get('/',(req,res) => {
    res.render('index')
})

// 确认登录，检查是否有用户名并且昵称不用重复
app.get('/check',function (req,res) {
    var account = req.query.account
   if(!account) {
       res.send('必须填写用户名')
       return;
   }
   if(alluser.indexOf(account) !== -1) {
        res.send('用户名被占用')
       return;
   }
   alluser.push(account)
    // 给予session
    req.session.account = account;
    res.redirect('/chat')

})

//聊天室
app.get('/chat',function (req,res,next) {
    // 必须保证有用户名
    if (!req.session.account){
        res.redirect('/')
        return
    }
    res.render('chat',{
        account: req.session.account
    })
})

io.on('connection',function (socket) {
    socket.on('chat',function (msg) {
        console.log(msg)
        // 把结束的msg原样转出
        io.emit('chat',msg)
    })
})

http.listen(3000)