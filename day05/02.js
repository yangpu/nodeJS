/**
 * Created by Nick on 2017/5/4.
 */
var express = require('express')
var db = require('./model/db')
var app = express()

app.get('/',(req,res) => {
    var name = req.query.name
    db.insertOne('student',{name: name, age: parseInt(Math.random()*100)},(err,result) => {
        if(err) {
            console.log('插入失败')
            return
        }
        res.send('插入成功')
    })

})

app.get('/du',(req,res) => {
    // 这个页面现在接受一个page参数？
    var page = parseInt(req.query.page)
    // db.find('student',{},{'pageamount':5,'page':page},(err,result) => {
    db.find('student',{},(err,result) => {

        if (err) {
            console.log(err)
        }
        res.send(result)
    })
})
app.get('/shan',(req,res) => {
    // 全部删除
    var name = req.query.name
    db.deleteMany('student',{'name': name},(err,result) => {

        if (err) {
            console.log(err)
        }
        res.send(result)
    })
})

// 修改
app.get('/xiugai',(req,res) => {
    var name = req.query.name
    db.updateMany('student',{'name': 'jjk'},{$set:{'name':name}},(err,result) => {

        if (err) {
            console.log(err)
        }
        res.send(result)
    })
})
app.listen(3000)