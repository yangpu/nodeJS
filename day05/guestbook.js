/**
 * Created by Nick on 2017/5/11.
 */
var express = require('express')
var db = require('./model/db')
var formidable = require('formidable')
var ObjectId = require('mongodb').ObjectID;
var app = express()

// 设置模板引擎
app.set('view engine','ejs')

// 静态
app.use(express.static('./public'))

// 显示留言列表
app.get('/',(req,res,next) => {

    db.getAllCount('guestbook',function (count) {
        res.render('index',{
            pagecount: Math.ceil(count/4)
        })
    })
})
app.get('/du',(req,res) => {
    var page = parseInt(req.query.page)
    db.find('guestbook',{},{'sort':{'time': -1},'pageamount':4,'page':page},(err,result) => {
        res.json({result:result})
    })
})

// 处理留言
app.post('/tijiao',(req,res,next) => {
    var form = new formidable.IncomingForm()
    form.parse(req,(err,fields) => {
        console.log(`收到请求了${fields.account}${fields.msg}`)
        // 插进数据库里面
        db.insertOne('guestbook',{
            account: fields.account,
            msg:fields.msg,
            time:new Date()
        },(err, result) => {
            if (err) {
                res.json('-1')
                return
            }
            res.json('1')
        })
    })
})

app.get('/del',function (req,res) {
    var id = req.query.id
    db.deleteMany('guestbook',{'_id':ObjectId(id)},function (err,result) {
        res.redirect('/')
        // res.send(result)
    })
})
app.listen(3000)