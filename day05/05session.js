/**
 * Created by 13258 on 2017/5/13.
 */
var express = require('express')
var session = require('express-session')

var app = express()

app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
    // cookie:{secure: true}
}))

app.get('/',function (req,res) {
    // 缓存在服务器中，一旦服务器关了，缓存就消失
    if (req.session.login) {
        res.send('欢迎您'+req.session.userName)
    } else {
        res.send('你没有登录')
    }



})
app.get('/login',function (req,res) {
    // 设置这个session
   req.session.login = true;
    req.session.userName = 'Kaola'
    res.send('你已经成功登录')

})
app.listen(3000)