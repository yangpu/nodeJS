/**
 * Created by Nick on 2017/5/8.
 */
// 封装了所有数据库的操作
var MongoClient = require('mongodb').MongoClient
var setting = require('./setting.js')

// 封装成内部函数
function _connectDB(callback) {
    var url = setting.dburl;
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('连接不成功')
            console.log(err)
            callback(err, null)
        }
        console.log('连接成功')
        callback(err, db)
    })
}

// 插入数据dao
exports.insertOne = function (collectionName, json ,callback) {
    _connectDB((err,db) => {
        db.collection(collectionName).insertOne(json,(err,result) => {
            callback(err,result)
            // 关闭数据库
            db.close()
        })
    })
}

// 查找数据，找到所有的数据.arge是个对象  {'pageamount':5,'page':page}
exports.find = function (collectionName,json,C,D) {
    var result = []
    if (arguments.length == 3){
        // 如果参数c就是callback，参数D就没有传
        var callback = C
        // 应该省略的条数
        var skipnumber = 0;
        // 数目限制
        var limit = 0;
        var sort =  {}
    } else if (arguments.length == 4) {
        var callback = D
        var args = C
        // 应该省略的条数
        var skipnumber = args.pageamount * args.page || 0;
        // 数目限制
        var limit = args.pageamount || 0;
        // 排行方式
        var sort = args.sort || {}
        console.log(sort)
    } else {
        throw err('参数必须4个')
        return
    }


    var json = json || {}
    _connectDB((err,db) => {
        var cursor = db.collection(collectionName).find(json).skip(skipnumber).limit(limit).sort(sort);
        cursor.each((err, doc) =>{
            if (err) {
                console.log(11111)
                callback(err, null)
                // 关闭数据库
                db.close()
                return
            }
            if (doc != null) {
                result.push(doc)
            }else {
                // 遍历结束，没有更多的文档了
                callback(null,result)
                // 关闭数据库
                db.close()
            }
        })
    })
}

// 删除函数
exports.deleteMany = function (collectionName,json,callback) {
    _connectDB((err,db) => {
        // 删除？
        db.collection(collectionName).deleteMany(json, (err, results) => {
            callback(err, results)
            // 关闭数据库
            db.close()
        })
    })
}

// 修改
exports.updateMany = function (collectionName,json1,json2,callback) {
    _connectDB((err,db) => {
        db.collection(collectionName).updateMany(json1,json2,(err, result) => {
            callback(err, result)
            // 关闭数据库
            db.close()
        })
    })
}

exports.getAllCount = function (collectionName,callback) {
    _connectDB(function (err,db) {
        db.collection(collectionName).count({}).then(function (count) {
            callback(count)
            console.log(count)
            db.close()
        })

    })
}

