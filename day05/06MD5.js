/**
 * Created by 13258 on 2017/5/13.
 */
var express = require('express')
var session = require('express-session')
var db = require('./model/db')
var session = require('express-session')

var app = express()

app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
    // cookie:{secure: true}
}))

app.set('view engine', 'ejs')
// 静态
app.use(express.static('./public'))

app.get('/login',function (req,res) {
    // 设置这个session
    res.render('login')

})
app.get('/checkLogin',function (req,res) {
    var account = req.query.account;
    var pwd = req.query.pwd;
    // 根据用户填写的姓名，去数据库里读取密码，
    // 如果密码一样则登录成功
    db.find('users',{'account':account},function (err,result) {
        if (result.length === 0) {
            res.send('登录名错了，没有这个东西')
            return;
        }
        if (pwd === result[0].pwd){
            req.session.login = '1'
            req.session.userName = result[0].account
            res.send('登录成功，你是'+result[0].account)
        }else {
            res.send('密码错误')
        }
    })
})
app.get('/',function (req,res) {
    if (req.session.login === '1') {
        res.send('欢迎您'+req.session.userName)
    } else {
        res.send('你没有登录')
    }
})
app.listen(3000)