/**
 * Created by 13258 on 2017/5/13.
 */
var express = require('express')
var cookieParser = require('cookie-parser')

var app = express()
// 使用cookie必须使用cookie-parser中间件
app.use(cookieParser())

app.get('/',function (req,res) {

    // maxAge在express以毫秒为单位
    res.cookie('name','tyboy',{maxAge:900000,httpOnly:true})
    res.send(req.cookies.target)

})
app.get('/plane',function (req,res) {
    // 得到get请求，用户查询的母的地
    var target = req.query.target

    // 记录用户的喜好
    // 先读取用户的喜好，然后把新的数据push
    // 进入数组，然后设置新的cookie
    var targetArr = req.cookies.target || []
    targetArr.push(target)
    res.cookie('target',targetArr)
    res.send(target + '旅游攻略')

})
app.listen(3000)