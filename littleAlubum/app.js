/**
 * Created by 13258 on 2017/4/27.
 */
var express = require('express')
var router = require('./controller')
var app = express()

// 设置模板引擎
app.set('view engine','ejs')

// 路由中间件，水管一样往下流，没有接住就往下流
// 静态页面,static往下流
app.use(express.static('./public'));
app.use(express.static('./uploads'));
// app.use('static',express.static('./public'));

// 不需要带req和res，相当于直接把函数提出来
app.get('/',router.showIndex)
app.get('/:albumName',router.showAlbum)
app.get('/up',router.showUp)
app.post('/up',router.dopost)

//404
app.use((req,res) => {
    res.render('err')
})

app.listen(3000)
