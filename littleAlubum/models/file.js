/**
 * Created by 13258 on 2017/4/27.
 */
var fs = require('fs')
// 异步回调函数
exports.getAllAlbums = function (callback) {
    fs.readdir('./uploads',(err,files) => {
        if (err){
            callback('找不到文件', null)
        }
        var allAlbums = [];
        // 函数自运行必须要有；
        (function iterator(i) {
            if (i == files.length) {
                console.log(allAlbums)
                callback(null,allAlbums)
                return
            }
            fs.stat('./uploads/'+files[i],(err, stats) => {
                if (err){
                    callback('找不到文件'+files[i], null)
                }
                if (stats.isDirectory()){
                    allAlbums.push(files[i])
                }
                iterator(i+1)
            })
        })(0)
    })
}

// 通过文件名，得到所有图片
exports.getPhoto = function (albumName,callback) {
    fs.readdir('./uploads/'+albumName,(err,files) => {
        if (err){
            callback('找不到文件', null)
            return
        }
        var allAlbums = [];
        // 函数自运行必须要有；
        (function iterator(i) {
            if (i == files.length) {
                console.log(allAlbums)
                callback(null,allAlbums)
                return
            }
            fs.stat('./uploads/'+albumName+'/'+files[i],(err, stats) => {
                if (err){
                    callback('找不到文件'+files[i], null)
                    return
                }
                if (stats.isFile()){
                    allAlbums.push(files[i])
                }
                iterator(i+1)
            })
        })(0)
    })
}
