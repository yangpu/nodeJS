/**
 * Created by 13258 on 2017/4/27.
 */
// 首页
var file = require('../models/file')
var formidable = require('formidable')
var path = require('path')
var fs = require('fs')
var dateTime = require('silly-datetime');

exports.showIndex = function (req,res,next) {
    // 这就是nodejs的编程思维，就是所有东西都是异步的
    // 所以内层的函数，不是return回来的东西，而是调用高层函数提供的
    // 回调函数。把数据当作回调函数的参数来使用
    file.getAllAlbums((err,allbumns) => {
        if (err){
            next()
            return
        }
        res.render('index',{
            'albumn': allbumns
        })
    })
}

// 相册页
exports.showAlbum = function (req,res,next) {
    var pathName = req.params.albumName
    // 遍历每张图片，具体的逻辑交给model来使用
    file.getPhoto(pathName,(err,imageArray) => {
        if (err){
            next()
            return
        }
        res.render('albumn',{
            photoName: pathName,
            image:imageArray
        })
    })

}

// 上传
exports.showUp = function (req,res) {
    file.getAllAlbums((err,allbumns) => {
        if (err){
            next()
            return
        }
        res.render('up',{
            'files': allbumns
        })
    })
}

//提交
exports.dopost = function (req,res) {
    var form = new formidable.IncomingForm()
    form.uploadDir = path.normalize(__dirname + '/../temp')
    form.parse(req, (err,fields,files) => {
        console.log(fields)
        console.log(files)
        // 改名
        if (err){
            next()
            return
        }

        // 判断文件尺寸
        var size = parseInt(files.photo.size);
        if (size>1000024){
            res.send('图片少于1M')
            fs.unlink(files.photo.path)
            return
        }

        var extname = path.extname(files.photo.name)
        var ttt = dateTime.format(new Date(),'YYYYMMDDHHmm')
        var ran = parseInt(Math.random()*88888+10000)

        var wenjianjia = fields.wenjianjia
        var oldpath = files.photo.path
        var newpath = path.normalize(__dirname + '/../uploads/'+wenjianjia+'/'+ttt + ran + extname)
        fs.rename(oldpath,newpath,(err) => {
            if (err){
                res.send('改名失败')
                return
            }
            res.end('success')
        })
    })

}