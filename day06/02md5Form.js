/**
 * Created by 13258 on 2017/5/14.
 */
var express = require('express')
var app = express()
var formidable = require('formidable')
var db = require('./model/db')
var md5 = require('./model/MD5')


app.use(express.static('./public'))
app.set('view engine','ejs')
app.get('/register',function (req,res) {
    res.render('register')
})
app.get('/login',function (req,res) {
    res.render('login')
})
// 执行注册
app.post('/dopost',function (req,res) {
    var form =  new formidable.IncomingForm();
    form.parse(req, function (err,fields,files) {
        console.log(fields.account,fields.pwd)
        //加密
        var pwd = md5(md5(fields.pwd).substr(4,7) + md5(fields.pwd))
        // 把用户名和密码存入数据库
        db.insertOne('users',{
            account:fields.account,
            pwd:pwd
        },function (err,result) {
            if (err){
                res.send('state:304')
            }else {
                res.send('state:200')
            }
        })
    })
})

// 执行注册
app.post('/doLogin',function (req,res) {
    var form =  new formidable.IncomingForm();
    form.parse(req, function (err,fields,files) {
        console.log(fields.account,fields.pwd)
        //加密
        var pwd = md5(md5(fields.pwd).substr(4,7) + md5(fields.pwd))
        // 检索数据库
        db.find('users',{
            account:fields.account
        },function (err,result) {
            if (result.length === 0) {
                res.send('没有改用户')
                return
            }
            var md5pwd = result[0].pwd
            // 加密数据进行比对
            if (pwd === md5pwd) {
                res.send('登录成功')
            }else {
                res.send('密码错误')
            }
        })
    })
})
app.listen('3000')

