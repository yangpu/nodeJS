/**
 * Created by 13258 on 2017/4/23.
 */
var express = require('express');

var app = express()

app.get('/',(req,res) => {
    res.send('你好')
})

app.get('/haha',(req,res) => {
    res.send('这是哈哈哈哈哈哈页面')
})

// 支持正则表达式
app.get(/^\/student\/([\d]{10})/,(req,res) => {
    res.send('学生信息，学号' + req.params[0])
})

app.get('/teacher/:gonghao',(req,res) => {
    res.send('老师信息，工号' + req.params.gonghao)
})
app.listen(3000)
