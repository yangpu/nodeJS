/**
 * Created by Nick on 2017/4/27.
 * post请求的参数
 * 前台：用户与界面的交互
 * 后台：数据，登录上传
 * cookie
 */
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.set('view engine','ejs')
app.use(bodyParser.urlencoded({ extended: true }));
// for parsing application/x-www-form-urlencoded

app.get('/',(req,res) => {
    res.render('from')
})

// 必须通过模板引擎引包
app.post('/',(req,res) => {
    console.log(req.body)
    // { name: 'ddddddd', age: 'ddddddddddd' }获取表单的属性
})

app.listen(3000)