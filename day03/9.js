/**
 * Created by 13258 on 2017/4/26.
 */
var express = require('express');
var fs = require('fs')
var app = express();

app.use(haha)
app.get('/admin',(req,res)=>{
    res.send('管理员')
})
app.listen(3000)

function  haha(req,res,next) {
    // 根据当前网站，读取public文件夹中的文件
    // 如果有这个文件，那么就渲染这个文件
    // 如果没有这个文件，就next
    var filepath = req.originalUrl;
    fs.readFile(`./public/${filepath}`,(err,data) => {
        if (err) {
            // 文件不存在
            next()
            return
        }
        res.send(data.toString())
    })

}