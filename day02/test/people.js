/**
 * Created by 13258 on 2017/4/20.
 */
// 构造函数暴露一个类的方法
function People(name,sex,age) {
  this.name = name;
  this.sex = sex;
  this.age = age;
}
People.prototype = {
    sayHello () {
        console.log(this.name)
    }
}

// 不能用exports,此时People就被new
module.exports = People