/**
 * Created by 13258 on 2017/4/23.
 * 上传改名
 */
var http = require('http')
var formidable = require('formidable')
var util = require('util')
var fs = require('fs')
var dateTime = require('silly-datetime');
var path = require('path')

http.createServer((req,res) => {
    //如果是访问地址是这个，并且请求类型是post
    if (req.url === '/dopost' && req.method.toLowerCase() === 'post') {
        var form = new formidable.IncomingForm();
        //设置图片上传地址
        form.uploadDir = './uploads'
        form.parse(req, (err,fields,files) => {
            console.log(util.inspect({filelds:fields,files:files}))
            //时间
            var ttt = dateTime.format(new Date(),'YYYYMMDDHHmm')
            var ran = parseInt(Math.random()*88888+10000)
            var extname = path.extname(files.picture.name)

            var oldpath = __dirname + '/' +files.picture.path;
            var newpath = __dirname + '/uploads/' +ttt + ran + extname;

            //改名
            fs.rename(oldpath,newpath,err => {
                if (err) {
                    throw err
                }
            })
            res.writeHead(200,{'content-type':'text/plain'})
            res.end('成功')
        })
    }

}).listen(3000,'127.0.0.1')
