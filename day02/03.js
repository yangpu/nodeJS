/**
 * Created by Nick on 2017/4/18.
 */
// nodejs能设计顶层路由
// 缓存问题永远都是200，需要cookie缓存，304是服务器给的
var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');
http.createServer((req, res) => {

    // 如果不用req.url来if判断，用户不管做什么都一样
    // 得到地址
    // normalize把多个/变成一个
    var pathname = url.parse(req.url).pathname;

    // 1.判断此时用户输入的是文件夹地址还是文件地址
    // 如果是文件夹地址，那么就自动访问里面的index.html
    if (!pathname.includes('.')) {
        pathname += './index.html'
    }
    // 输入的网站是localhost
    // console.log(pathname)
    var fileURL = './' + path.normalize(`./static/${pathname}`);

    // 得到拓展名
    var extname = path.extname(pathname)


    fs.readFile(fileURL, (err, data) => {
        // 2.404页面文件不存在
        if (err) {
            res.writeHead(404,{'Conent-Type':'text/html;charset:UTF8'})
            res.end('404,页面没有找到')
            return;
        }

        // 读完之后做的事情
        getMime(extname,(mime = 'text/plain')=> {
            res.writeHead(200, {'Content-Type': mime + ';charset:UTF8'});
            res.write(data);
            res.end()
        })

    })
}).listen(3000, 'localhost')

function getMime(extname,callback) {
    // switch (extname){
    //     case '.html':
    //         return 'text/html'
    //         break;
    //     case '.css':
    //         return 'text/css'
    //         break;
    //     case '.js':
    //         return 'text/javascript'
    //         break;
    // }
    // 读取mime文件，得到json，根据extname key，返回对应的value
    // 读取文件，类似ajax，异步的,所以要callback
    fs.readFile('./mime.json',(err,data) => {
        if (err) {
            throw Error('找不到文件');
            return;
        }
        //转成 json
        var mimeJSON = JSON.parse(data);
        console.log(mimeJSON[extname])
        callback(mimeJSON[extname])
    })
}
