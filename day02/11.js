/**
 * Created by 13258 on 2017/4/23.
 */
var http = require('http')
var querString = require('querystring')

http.createServer((req,res) => {
    //如果是访问地址是这个，并且请求类型是post
    if (req.url === '/dopost' && req.method.toLowerCase() === 'post') {
        //下面是post请求的一个，node为了追求极致，它是一个小段一个小段接受的
        //接受了一个小段，可能去给别人服务
        var alldata = '';
        req.addListener('data',chunk => {
            alldata += chunk;
        })

        req.addListener('end',() => {

            var dataString = alldata.toString()
            res.end('success')
            var toObj = querString.parse(dataString)
            console.log(toObj)
            console.log(toObj.acount)
            console.log(toObj.sex)
        })
    }

}).listen(3000,'127.0.0.1')
